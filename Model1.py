# %% [markdown]
# Загружаем данные

# %%
import numpy as np
from tensorflow import keras as ks
import tensorflow as tf
import pickle as cPickle

# %%
def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict

# %%
ds=unpickle("train")
ds.keys()

# %%
xtrain=ds[b'data']
x_train=xtrain.reshape(50000,3,32,32)
x_train=np.transpose(x_train,(0,2,3,1))

yltrain=ds[b'fine_labels']
y_train=np.reshape(yltrain,(50000,1))
ycstrain=ds[b'coarse_labels']
yc_train=np.reshape(ycstrain,(50000,1))
x_train.shape

# %%
ds=unpickle("test")
ds.keys()

# %%
xtest=ds[b'data']
x_test=xtest.reshape(10000,3,32,32)
x_test=np.transpose(x_test,(0,2,3,1))
yltest=ds[b'fine_labels']
y_test=np.reshape(yltest,(10000,1))
ycstest=ds[b'coarse_labels']
yc_test=np.reshape(ycstest,(10000,1))
x_test.shape

# %%
#from tensorflow.keras.datasets import cifar100
#(x_train, y_train), (x_test, y_test) = cifar100.load_data(label_mode="fine")

# %%
import matplotlib.pyplot as plt 

# %%
plt.imshow(x_train[0])

# %%
x_train.shape

# %%
y_train.shape

# %%
y_train[0][0]

# %% [markdown]
# Нормализация данных

# %%
def normalize(ds):
    max=np.max(ds)
    min=np.min(ds)
    normalized=(ds-min)/(max-min)
    return normalized

# %%
x_train_norm=normalize(x_train)
x_test_norm =normalize(x_test)

# %%
np.min(x_test_norm)

# %% [markdown]
# Создание модели

# %% [markdown]
# 1) Объяснить, какие элементы вашей сети зависят от количества цветов, какие — от количества классов.
# 
# Входной слой (сверточный слой) зависит от количества цветов, выходной слой от количества классов

# %% [markdown]
# 2) Объяснить место в модели каждого слоя, обосновать выбор гиперпараметров
# 
# Цель сверточного слоя выделить особенности, по которым модель будет распознавать что изображено на картинке.
# Выбор оптимального размера ядра зависит от размытости картинки, ее размера, размер ядра выбирают нечетный так как легко определить центральный пиксель.
# С увеличением размера ядра увеличевается и сложность вычислений, поэтому стоит брать не слишком большой размер, например 3*3.
# Функция relu- часто используемая функция мало затратная для вычислений.
# Наилучшим для сети с одним сверточным слоем является число фильтров 32
# 
# Maxpooling нужен для уменьшения размера изображения, а соответственно требуемой вычислительной мощности
# 
# Flatten - уменьшение размерности вывода
# 
# 100 нейронов на выходе (100 классов) c функцией активации softmax которая, возвращает распределение вероятностей в сумме равное 1

# %%
model = ks.Sequential([
    ks.layers.Conv2D(filters=32,kernel_size=(3,3),input_shape=(32, 32,3),activation=tf.nn.relu),
    ks.layers.MaxPooling2D(pool_size=(2, 2)),
    ks.layers.Flatten(),
    ks.layers.Dense(100, activation=tf.nn.softmax)
])

# %%
model.compile(loss='sparse_categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

# %%
model.summary()

# %% [markdown]
# Тренировка модели

# %%
model.fit(x_train_norm, y_train,batch_size=100,epochs=10)

# %%
model.fit(x_train_norm, y_train,batch_size=100,epochs=10)

# %% [markdown]
# Вторая модель

# %% [markdown]
# Обучение второй модели

# %%
'''
model2 = ks.Sequential([
    ks.layers.Conv2D(filters=32,kernel_size=(3,3),input_shape=(32, 32,3),activation=tf.nn.relu),
    ks.layers.MaxPooling2D(pool_size=(2, 2)),
    ks.layers.Flatten(),
    ks.layers.Dense(20, activation=tf.nn.softmax)
])

# %%
model2.compile(loss='sparse_categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

# %%
model2.fit(x_train_norm, yc_train,batch_size=100,epochs=10)

# %%
model2.fit(x_train_norm, yc_train,batch_size=100,epochs=10)
'''
# %% [markdown]
# Сравнение результатов двух моделей

# %%
loss,accuracy=model.evaluate(x_test_norm, y_test, batch_size=32)

# %%
#loss,accuracy=model2.evaluate(x_test_norm, yc_test, batch_size=32)

#Сохранение модели
model.save("my_model.keras")
